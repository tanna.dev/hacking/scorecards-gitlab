// Copyright OpenSSF Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Command scorecard-action is the entrypoint for the Scorecard GitHub Action.
package main

import (
	"log"
	"os"

	"gitlab.com/tanna.dev/hacking/scorecards-gitlab/signing"
)

func main() {
	// fetch a pre-computed JSON report
	jsonPayload, err := os.ReadFile("report.json")
	if err != nil {
		log.Fatalf("error parsing json scorecard results: %v", err)
	}

	// Sign json results.
	// Requires the following in your GitLab CI for the job and at least cosign v2.0.1:
	//
	//   id_tokens:
	//     CI_JOB_JWT: {}
	//     SIGSTORE_ID_TOKEN:
	//       aud: sigstore
	accessToken := os.Getenv("CI_JOB_JWT")
	s, err := signing.New(accessToken)
	if err != nil {
		log.Fatalf("error SigningNew: %v", err)
	}
	if err = s.SignScorecardResult("report.json"); err != nil {
		log.Fatalf("error signing scorecard json results: %v", err)
	}

	// Processes json results.
	repoName := os.Getenv("CI_PROJECT_PATH")
	repoRef := os.Getenv("CI_COMMIT_SHA")
	if err := s.ProcessSignature(jsonPayload, repoName, repoRef); err != nil {
		log.Fatalf("error processing signature: %v", err)
	}
}
